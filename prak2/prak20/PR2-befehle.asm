	ORG 100H

;----------------------------------------

; PRAKTIKUM 2:
; Grundlegende Befehle

start:
       mov al,170           ; lege Wert 170 in al ab
       out 0,al             ; sende wert aus al an output 0 (LEDs)
       mov ah,01010101b     ; 85 in ah schreiben
       mov cx,ax            ; bewege den wert von ax nach cx

       mov [0200h],al   ; lege wert von al in Speicheradresse 0200h ab
       mov [0202h],cx

       add ch,60h     ; f�ge zu ch 60h hinzu

       mov bx,2   ; bx := 2
       dec bx     ; bx--
       dec bx     ; bx--
       dec bx     ; bx--

       mov dx,7700h      ; 7700h in dx ablegen
       or  dl,00110011b  ; low byte von dx mit 00110011b verkn�pfen
       and dh,11001100b  ; high byte von dx mit 11001100b maskieren

       mov al,00000110b ; schreibe 6 in al
       out 0,al         ; gebe wert von al nach output 0
       rol al,1         ; rotiere al 1 mal nach links (mal 2)
       out 0,al         ; gebe 12 an output 0
       rol al,1         ; ...
       out 0,al         ; ...
       rol al,1         ; ...

       mov bl,00000110b  ; schreibe 6 nach bl
       mov cl,3          ; schreibe 3 nach cl
       rol bl,cl         ; rotiere bl 3 mal nach links (mal 6 = 36)

       mov al,-1   ; schreibe -1 in al
       out 90h,al  ; gebe al an output 135 (erste Ziffer Display)
       not al      ; negiere (invertiere) al
       out 90h,al  ; gebe al nochmal output 135

       jmp start   ; springe an marke "start"


;----------------------------------------
