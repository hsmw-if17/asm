          ORG 100H

; HAUPTPROGRAMM
start:    call clrdsp   ;display l�schen

          ;schalter einlesen und ausgeben
          in al, 0
          mov dl, [dv]
          call anz2st

          mov bx, 0h     ;bx als z�hlvariable

          ;gebe bx auf display aus
          push ax       ;ax zwecks funktionsaufrufs speichern
          mov ax, bx    ;ax mit z�hlvariable laden
          mov dl, [dc]
          call anz4st   ;bx auf 4 displaystellen ausgeben
          pop ax        ;ax wiederherstellen

          ;Speicherbereich durchsuchen
          mov si, [sb]  ;anfang

search:   mov cl, [si]  ;wert aus speicher holen
          cmp cl, al    ;vergleiche mit gesuchtem wert
          jne skip      ;falls ungleich, gehe zu "skip"

          inc bx        ;sonst, erh�he bx

          ;gebe bx auf display aus
          push ax
          mov ax, bx
          call anz4st
          pop ax

skip:     inc si         ;erh�he si
          cmp si, [se]   ;vergleiche mit ende
          jne search     ;wiederhole falls noch nicht da
          call warte     ;warte, damit ergebnis sichtbar
          call warte
          call warte
          jmp start      ;sonst, wiederhole gesamtes programm

; UNTERPROGRAMME

;wartet kurz - keine parameter
warte:
      push cx
      mov cx, 0ffffh
      l: loop l
      pop cx
      ret

;display l�schen - keine Parameter
clrdsp:
          ;verwendete register retten
          push dx
          push ax

          ;display 90h-9eh mit 0 laden
          mov dx, 90h
          mov ax, 0
   clr:   out dx, ax
          add dx, 2
          cmp dx, 0a0h
          jne clr

          ;verwendete register wiederherstellen
          pop ax
          pop dx
          ret


;anzeige von al (0-f) auf stelle dx
anz1st:
          push ax
          push bx

          mov bx, ax       ; alten wert von ax in bx laden
          ;bl kann zur addressierung nicht verwendet werden, nur bx
          ;also bx so anpassen, dass nur gesuchter wert bleibt
          and bx, 000fh      ; nur untere 4 bits (0-f) betrachten

          mov al, [bx+table]  ;entsprechenden wert aus tabelle holen
          out dx, al       ; wert auf dx ausgeben

          pop bx
          pop ax
          ret


;anzeige von al (00-ff) auf stelle dx und dx+2
anz2st:
       push ax
       push dx

       call anz1st ;erste vier bit von al auf dx ausgeben

       ;obere 4 bit zu ersten 4 bit machen
       shr al,1
       shr al,1
       shr al,1
       shr al,1

       ;n�chste displayadresse ausw�hlen
       add dx, 2

       ;wieder erste vier bit von al auf dx ausgeben
       call anz1st

       pop dx
       pop ax
       ret


;anzeige von ax auf 4 stellen ab dx
anz4st:
       push ax
       push dx

       ;gebe al auf dx, dx+2 aus
       call anz2st

       mov al, ah
       add dx, 4
       call anz2st

       pop dx
       pop ax
       ret

;VARIABLENDEFINITION
sb:         dw 0c000h ;von 0c000h   ;SearchBegin
se:         dw 0d000h ;bis 0d000h durchsuchen     ;SearchEnd
dv:         db 9ch    ;displaystellen suchwert ;DisplayValue
dc:         db 90h    ;displaystellen z�hlwert ;DisplayCount

;DISPLAYTABELLE
table:      db  00111111b       ;0
            db  00000110b       ;1
            db  01011011b       ;2
            db  01001111b       ;3
            db  01100110b       ;4
            db  01101101b       ;5
            db  01111101b       ;6
            db  00000111b       ;7
            db  01111111b       ;8
            db  01101111b       ;9
            db  01110111b       ;A
            db  01111100b       ;B
            db  01011000b       ;C
            db  01011110b       ;D
            db  01111001b       ;E
            db  01110001b       ;F