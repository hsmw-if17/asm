          ORG 100H
          
          ;alle LEDS aus
start:    mov al, 0
          out 0, al
          
          ;gib 0000 0001 auf LEDs aus
          mov al, 1
          out 0, al
          call warte

          ;rotiere links <-
          ;bis al = 1000 0000
left:     rol al, 1
          out 0, al
          call warte
          cmp al, 128
          jnz left

          ;rotiere rechts ->
          ;bis al = 0000 0001
right:    ror al, 1
          out 0, al
          call warte
          cmp al, 1
          jnz right

          ;rotiere wieder links
          jmp left

          ;warteschleife
warte:    mov cx, 0ffffh
schleife: loop schleife
          ret