          ORG 100H
          
          ; reset
          mov al, 0
          out 0, al

start:    in   al, 0 ;schalter einlesen (0000 000x)
          test al, 1
          jnz  leds
          jmp  start

leds:     mov al, 0ffh
          out 0, al
          jmp start