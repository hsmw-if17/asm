
	  ORG 100H
;----------------------------------------

; PRAKTIKUM 3:
; Flags und bedingte Spruenge

start:
; Sagen Sie voraus, welche Flags (CF-ZF-PF-SF) und wie durch die
; entsprechenden Befehle beeinflusst werden, Ueberpruefen Sie

          mov  al,81h       ;  10000001
          cmp  al,0c0h      ; +01000000 (-11000000)
                            ; =11000001
                            ; sign
          sub al,0c0h
          mov al,81h

          test al,7eh       ; 01111110 & 10000001 = 0
                            ; zero, parity

          or   al,18h       ; 00011000 | 10000001 = 10011001
                            ; parity, sign

          and  al,0fh       ; 00001111 & 10011001 = 00001001
                            ; parity

          mov  ah,2         ; ah = 00000010
          ror  ah,1         ; ah = 00000001 (keine flags)
          ror  ah,1         ; ah = 10000000 (carry, sign flag)

eingc:    in   al,0    ; Schalter abfragen
          cmp  al,9
          jc  eingc    ; bei welchen Eingabewerten wird gesprungen ?
                       ; 0-8

eingt:    in   al,0    ; Schalter abfragen
          test al,81h  ; 1000 0001
          jnz  eingt   ; bei welchen Eingabewerten wird gesprungen ?
                       ; 1xxx xxxx
                       ; 1xxx xxx1
                       ; xxxx xxx1
          nop
          jmp start

;----------------------------------------
