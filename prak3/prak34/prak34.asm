          ORG 100H

          ;7seg. anzeige geht von 90h 92h 94h ... 9eh
          ;l�sche gesamte 7seg. anzeige
          mov dx, 90h
          mov al, 0
clear:    out dx, al
          add dx, 2h
          cmp dx, 9eh
          jne clear


start:    in al, 0     ;schalter einlesen
          test al, 8h  ;schalter 3 ausw�hlen
          mov dx, 96h  ;7seg. anzeige an stelle 3 �ndern
          jz skip3     ;wenn schalter aus, �berspringe einschalten
          call an      ;sonst einschalten
          jmp goto4    ;und weiter zu schalter 4

skip3:    call aus     ;schalter war aus, 7seg. anzeige auf "0"

goto4:    test al, 10h ; schalter 4 ausw�hlen
          mov dx, 98h  ;7seg. anzeige an stelle 4 �ndern
          jz skip4     ;wenn schalter aus, �berspringe einschalten
          call an      ;sonst einschalten
          jmp start    ;und wiederholen

skip4:    call aus     ;schalter war aus, 7seg. anzeige auf "0"
          jmp start    ;und wiederholen


;7seg. anzeige an stelle [dx] auf "1"
an:       push ax
          mov al, 0000110b
          out dx,al
          pop ax
          ret

;7seg. anzeige an stelle [dx] auf "0"
aus:      push ax
          mov al, 00111111b
          out dx,al
          pop ax
          ret