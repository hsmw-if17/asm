          ORG  100H
          
          ;alle LEDs ausschalten
clear:    mov  al, 0
          out  0, al     

start:    in   al, 0     ;Schalter einlesen
          test al, 1     ;nach Schalter 0000 000x testen
          jz   start     ;tue nichts, wenn schalter aus

          ;zweiten Schalter auswerten
          mov ch, al     ;schreibe schaltereingabe in CH
          and ch, 80h    ;nur den obersten Schalter ausw�hlen
          or  cx, 7FFFh  ;alles (au�er den schalter) auf 1 setzen
          ;=> wenn schalter ein CX = 1111 1111
          ;=> wenn schalter aus CX = 0111 1111
          ; CX als warte-variable
          ; => geschwindigkeitsunterschied

          ;alle LEDs anschalten
          mov al, 0FFh
          out  0, al

          call warte

          ;alle LEDs ausschalten
          mov al, 0
          out  0, al

          call warte

          ;wiederhole
          jmp  start


          ;unterprogramm, das zeit verschwendet
          ;cx schon weiter oben ausgewertet
warte:    loop warte
          ret