            ORG 100H


start:      mov bl,0         ;clear
            in al, 0         ;schalter einlesen
            test al, 080h    ;schalter 7 pr�fen
            jz skipleft      ;schalter aus, �berspringen
            or bl,0f0h       ;links ein

skipleft:   test al, 1       ;schalter 0 pr�fen
            jz skipright     ;schalter aus, �berspringen
            or bl,0fh        ;rechts ein

skipright:  mov al,bl        ;eingabe ausgeben
            out 0,al
            call warte       ;warten
            mov al,0
            out 0,al         ;ausschalten ("blinken")
            call warte
            jmp start        ;zur�ck an den start

warte:    mov cx, 0ffffh
schleife: loop schleife
          ret