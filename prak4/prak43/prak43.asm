            ORG 100H
            
            ;7-seg anzeige l�schen
start:      mov dx, 90h
            mov al, 0
clear:      out dx, al
            add dx, 2h
            cmp dx, 9eh
            jne clear
            jmp read
            
read:       in  al, 0
            and al, 0fh
            mov si, ax
            mov al, [tabelle+si]
            out 90h, al
            jmp read

tabelle:    db  00111111b
            db  00000110b
            db  01011011b
            db  01001111b
            db  01100110b
            db  01101101b
            db  01111101b
            db  00000111b
            db  01111111b
            db  01101111b
            db  01110111b
            db  01111100b
            db  01011000b
            db  01011110b
            db  01111001b
            db  01110001b