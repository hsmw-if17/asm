               ORG 100H
               
               jmp p411
;               jmp p412
;               jmp p413

;schreibe in 200h die eingelesenen schalter
p411:          mov si, 200h        ;si zeiger auf adresse 200h
               in al, 0            ;schalter einlesen
p411a:         mov [si],al         ;schreibe schalter in speicher an stelle dx
               inc si
               cmp si, 300h
               jne p411a
               jmp p411

p412:          mov si, 200h        ;si zeiger auf adresse 200h
               mov al, 0           ;al = 0
p412a:         mov [si,al         ;speicher an adresse si = al
               inc al              ;al++
               inc si              ;si++
               cmp si, 300h        ;while(si<300h)
               jne p412a           ;repeat



p413:          mov si, 200h        ;si zeiger auf adresse 200h
p413a:         mov al, [si]        ;al = speicher an adresse si
               mov [si+100h], al   ;speicher an adresse si+100h = al
               inc si              ;si++
               cmp si, 300h        ;while(si<300h)
               jne p413a           ;repeat
