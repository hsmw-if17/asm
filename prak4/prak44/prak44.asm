          ORG 100H
          
            ;7-seg anzeige l�schen
            mov dx, 90h
            mov al, 0
clear:      out dx, al
            add dx, 2h
            cmp dx, 9eh
            jne clear
            jmp reset

reset:    mov si, tabelle    ; si zeiger auf adresse "tabelle"
start:    in al, 0           ; schalter einlesen
          test al, 1         ; ersten schalter abfragen
          jnz wuerfeln       ; wenn an, gehe zu wuerfeln
          jmp start          ; lies schalter nochmal

wuerfeln: mov al, [si]       ; lies in AL den auszugebenden wert
          out 90h, al        ; gib den wert auf der 7seg anzeige aus
          inc si             ; erh�he si um 1
          cmp si, tabelle+6  ; falls tabellengrenze ueberschritten
          je reset           ; setze si zur�ck
          jmp start          ;nochmal

tabelle:    db  00000110b ; 1
            db  01011011b ; 2
            db  01001111b ; 3
            db  01100110b ; 4
            db  01101101b ; 5
            db  01111101b ; 6