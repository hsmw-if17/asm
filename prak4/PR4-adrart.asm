
        ORG 100H
;----------------------------------------

; PRAKTIKUM 4:
; Adressierungsarten

; Kommentieren (Beschreiben) Sie folgende Befehle bzw.
; Programmablaeufe!
; Welche konkreten Zahlenwerte werden gelesen bzw.
; geschrieben?
; Was ist der Unterschied zwischen EQU und DB?
; Wie zeigen sich die so definierten Werte im fertigen
; Maschinencode ?

;equ ist wie "#define" - marke wird w�hrend compilation durch wert ersetzt
anfa:	equ 0200h	; Anfang
ende:	equ 02ffh	; Ende
anza:	equ ende-anfa+1	; "l�nge" von 0x200-0x2ff == 0x100
scha:	equ 0		; Adresse Schalter
anz0:	equ 90h		; Adresse rechte 7-Segm-Anzeige

start:
	mov al,[var8a]     ; al := 0x55 (01010101)
	not al             ; al := 0xAA (10101010)
	mov [var8a],al     ; var8a := 0xAA

	dec byte [var8b]  ; WARUM muss Datenbreite angegeben werden?
	                  ; A: variablengr��e (breite) nicht bekannt

                          ; andere Schreibweisen
	and [byte var8b],33h   ; 0xff & 0x33 == 0x33h

	mov bx,textanf    ; WAS wird nach BX geladen?   A: adresse von "textanf"
	mov cl,[bx]       ; ... und jetzt?              A: wert an adresse in bx == wert an adresse "textanf" == "A"
	mov ch,[bx+3]     ; ch := "a"

        mov al,01011100b
        mov dx,anz0      ; dx := 90h
        out dx,al        ; out 90h, al
        inc dx           ; dx := 91h
        inc dx           ; dx := 92h
	out dx,al        ; out 92h, al

;Zugriff auf eine Codetabelle
	mov cx,4          ; Z�hler Wiederholungen
	mov si,0
schl:	mov bx,codetab    ; bx = adresse von "codetab"
	mov al,[bx+si]    ; al := 00000110b
	out anz0,al       ; al auf anzeige ausgeben
	inc si            ; si++
	dec cx            ; cx--
	jnz schl          ; wiederhole ausgabe w�hrend cx nicht null ist (also 4 mal)

	jmp start         ;wiederhole gesamtes programm

        nop              ; Op-Code 90H
        nop
        nop

        ;d? reserviert speicher und legt wert ab
        ;db == byte (1 byte)
        ;dw == word (2 byte)
        ;dd == double word (4 byte)
var8a:	db 55h
var8b:	db 0
var16:	dw 1234h

textanf: db "ABCabc"
textend: db 00

codetab: db 00000110b	;1
	 db 01011011b	;2
	 db 01001111b	;3
	 db 01100110b	;4

;----------------------------------------

