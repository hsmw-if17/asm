          ORG 100H

start:    call clrdisp   ;display clearen
          mov bl, 0fh    ;bl und bh als
          mov bh, 0      ;z�hlvariablen verwenden

schl:     in al, 0       ; schalter einlesen
          test al, 40h   ; schalter 6 abfragen
          jz skip1       ; falls nicht gedr�ckt, �berspringen
          mov dx, 9ch    ; per dx, segment 6 ausw�hlen
          mov al, bl     ; in al unseren z�hler laden (nur al kann ausgegeben werden)
          call anz1st    ; gib aus
          dec bl         ; bl = bl-1
          jns skip1      ; wenn bl negativ
          mov bl, 0fh    ; setze bl zur�ck auf f

skip1:    in al, 0       ; lies schalter ein
          test al, 0ch   ; frage schalter 2 und 3 ab
          jz skip2       ; falls weder noch gedr�ckt, �berspringe
          mov dx, 94h    ; w�hle per dx segment 2
          mov al, bh     ; lade z�hler in al
          call anz2st    ; gib aus
          inc bh         ; bh = bh + 1
          cmp bh, 10h    ; wenn bh gr��er als f
          jl skip2       ; dann
          mov bh, 0      ; setze bh zur�ck auf 0

skip2:    call warte     ; warte
          jmp schl       ; alles nochmal


clrdisp:    push dx     ; save dx
            push ax     ; save ax
            mov dx, 90h ; erstes segment
            mov al, 0
clear:      out dx, al  ; gib nix auf segment aus
            add dx, 2h  ; dx = dx + 2
            cmp dx, 9eh ; falls nicht letztes segment
            jne clear   ; alles nochmal
            pop ax      ; restore ax
            pop dx      ; resotre dx
            ret


anz1st:     push ax ;sp+4
            push si ;sp+2
            push bx ;sp

            mov si, sp    ; kann [sp] nicht verwenden... also in si laden
            mov bx, [si+4]   ; lade alten ax wert in bx
            mov ax, [bx+tabelle]  ; suche den wert in tabelle
            out dx, ax      ; gib den wert aus

            pop bx
            pop si
            pop ax
            ret


anz2st:     push ax ;sp+4
            push dx ;sp+2
            push si ;sp
            mov si, sp       ; kann [sp] nicht verwenden, lade in si
            mov ax, [si+4]   ; lade alten ax wert in ax
            call anz1st      ; gib aus

            inc dx       ; w�hle n�chstes segment
            inc dx
            call anz1st  ; gib nochmal aus

            pop si
            pop dx
            pop ax
            ret

warte:    push cx
          mov cx, 0ffffh
schleife: loop schleife  ; cx--; if cx > 0: jmp schleife;
          pop cx
          ret

tabelle:    db  00111111b       ;0
            db  00000110b       ;1
            db  01011011b       ;2
            db  01001111b       ;3
            db  01100110b       ;4
            db  01101101b       ;5
            db  01111101b       ;6
            db  00000111b       ;7
            db  01111111b       ;8
            db  01101111b       ;9
            db  01110111b       ;A
            db  01111100b       ;B
            db  01011000b       ;C
            db  01011110b       ;D
            db  01111001b       ;E
            db  01110001b       ;F